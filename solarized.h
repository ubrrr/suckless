static const char background[]      = "#002b36";
static const char gray[]            = "#444444";
static const char white[]           = "#bbbbbb";
static const char border[]          = "#859900";
static const char *colors[][3]      = {
	/*               fg          bg          border   */
	[SchemeNorm] = { white,      background, gray },
	[SchemeSel]  = { background, border,     border },
};

static const char *tagsel[][2][2] = {
	//      norm                          sel
	//  fg          bg              fg          bg 
	{ { "#dc322f", background }, { "#dc322f", "#004a5d" } },
	{ { "#859900", background }, { "#859900", "#004a5d" } },
	{ { "#b58900", background }, { "#b58900", "#004a5d" } },
	{ { "#268bd2", background }, { "#268bd2", "#004a5d" } },
	{ { "#d33682", background }, { "#d33682", "#004a5d" } },
	{ { "#2aa198", background }, { "#2aa198", "#004a5d" } },
	{ { "#cb4b16", background }, { "#cb4b16", "#004a5d" } },
	{ { "#6c71c4", background }, { "#6c71c4", "#004a5d" } },
	{ { "#eee8d5", background }, { "#eee8d5", "#004a5d" } },
};
